<?php

require_once('widgets/class-wp-widget-categories.php');
//  Theme support

function photogenik_theme_setup () {
  // Featured Image Support
  add_theme_support('post-thumbnails');

  set_post_thumbnail_size(900, 600);

  // Post Format Support
  add_theme_support('post-formats', array (
    'gallery'
  ));

}

add_action('after_setup_theme', 'photogenik_theme_setup');

//Widgets Locations

function photogenik_init_widgets ($id) {
  register_sidebar(array(
    'name' => 'Sidebar',
    'id' => 'sidebar'
  ));
}

add_action( 'widgets_init', 'photogenik_init_widgets' );

//Register Widgets

function photogenik_custom_register_widgets () {
  register_widget('WP_Widget_Categories_Custom');
}

add_action('widgets_init','photogenik_custom_register_widgets');