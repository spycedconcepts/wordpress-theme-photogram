<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css" >
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php bloginfo('name'); ?></title>
    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>
    <!-- Header block-->
    <header class="w3-container w3-teal">
      <div class="w3-row">
        <div class="w3-col m9 l9">
          <a href = "\"><h1><?php bloginfo('name'); ?></h1></a>
        </div>
        <div class="w3-col m3 l3">
          <form method="get" action="<?php echo esc_url(home_url('/')) ?>">
            <input name="s" type="text" class="w3-input" placeholder="Search">
          </form>
        </div>
      </div>
    </header>

    <div class="w3-row">
      
      <!-- Menu Block --> 
      <div class="w3-col m3 l3">
        <?php if(is_active_sidebar('sidebar')) : ?>
          <?php dynamic_sidebar('sidebar'); ?>
      <?php endif; ?>
      </div>

      <!--Main Block-->
      <div class="w3-col m9 l9">
        <div class="w3-row">